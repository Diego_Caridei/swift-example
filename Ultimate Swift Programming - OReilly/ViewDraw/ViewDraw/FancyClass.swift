//
//  FancyClass.swift
//  ViewDraw
//
//  Created by Guybrush_Treepwood on 01/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

class FancyClass: UIView {

    override func awakeFromNib() {
        self.backgroundColor = UIColor.greenColor()
        
    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
