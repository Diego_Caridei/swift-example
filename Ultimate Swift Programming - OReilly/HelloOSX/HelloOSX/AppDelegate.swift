//
//  AppDelegate.swift
//  HelloOSX
//
//  Created by Guybrush_Treepwood on 29/05/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!

    @IBOutlet weak var textField: NSTextField!

    @IBAction func buttonClicked(sender: AnyObject) {
        self.textField.stringValue = "Hello World"
    }
    
    
    
    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

