//
//  ViewController.swift
//  Layer
//
//  Created by Guybrush_Treepwood on 03/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit
import QuartzCore


class ViewController: UIViewController {

    @IBOutlet weak var layerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layerView.layer.borderColor =  UIColor.redColor().CGColor
        layerView.layer.borderWidth = 3
        layerView.layer.shadowColor = UIColor.grayColor().CGColor
        layerView.layer.shadowOffset = CGSize(width: 50, height: 50)
        layerView.layer.shadowOpacity = 0.9
        
        let circle = CAShapeLayer()
        let path = UIBezierPath(ovalInRect: CGRect(x: 10, y: 10, width: 20, height: 20))
        circle.path = path.CGPath
        
        
        self.layerView.layer.addSublayer(circle)
        
        //Animation
        let animation = CABasicAnimation(keyPath: "transform")
        animation.fromValue = NSValue(CATransform3D: CATransform3DIdentity)
        
        //Rotazione
        let angle = CGFloat(M_PI_2) //Pi diviso due
        let rotation = CATransform3DMakeRotation(angle, 0.0, 0.0, 1.0)// anglo di rotazione e gl assi x,y,z
        
        animation.toValue = NSValue(CATransform3D:rotation)
        animation.duration = 1.0
        animation.autoreverses = true //avanti e indietro
        animation.repeatCount = HUGE //Si ripete per sempre
        self.layerView.layer.addAnimation(animation, forKey: "rotazione")
        
        
        //KeyframeAnimation
        let animationPositions = [
              NSValue(CGPoint: CGPoint(x: 50, y: 50)),
              NSValue(CGPoint: CGPoint(x: 150, y: 50)),
              NSValue(CGPoint: CGPoint(x: 150, y: 150)),
              NSValue(CGPoint: CGPoint(x: 50, y: 150)),
              NSValue(CGPoint: CGPoint(x: 50, y: 50)),
        ]
        
        //Abbiamo lo stesso numero di animationPosition e di animationTimes
        let animationTimes = [0.0, 0.5, 0.75, 0.9, 1.0]
        
        let keyframeAnimation = CAKeyframeAnimation(keyPath: "position")
        keyframeAnimation.values = animationPositions
        
        keyframeAnimation.keyTimes = animationTimes
        keyframeAnimation.duration = 1.0
        keyframeAnimation.repeatCount = HUGE
        
        
            layerView.layer.addAnimation(keyframeAnimation, forKey: "Spostamento")//la key serve a me per capire quale animazione
        
        
        
    }
    
    
    
    override func viewDidLayoutSubviews() {
        //CAShapeLayer ci consente di cambiare la forma di un layer
        let shapeLayer = CAShapeLayer()
        let path = UIBezierPath(ovalInRect: self.layerView.bounds)

        shapeLayer.path = path.CGPath
        self.layerView.layer.mask = shapeLayer
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

