//
//  ViewController.swift
//  GameControllerTest_MacOSX
//
//  Created by Guybrush_Treepwood on 07/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import Cocoa
import GameController
class ViewController: NSViewController {

    var gameController : GCController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func checkForController(sender: AnyObject) {
        
        let controller = GCController.controllers()
        
        println("Controllers: \(controller)")
        
        if controller.count > 0{
            gameController = controller[0] as? GCController
            self.gameController?.playerIndex = GCControllerPlayerIndexUnset
            
            if self.gameController?.extendedGamepad != nil{
                println("Found the extended controller!")
                var profile = self.gameController?.extendedGamepad
                profile?.rightTrigger.valueChangedHandler = {(input:GCControllerButtonInput!,value:Float,pressed: BOOl) -> Void in
                    println("right triggre pressed!")
            }
        }
        
     }
    }
    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

