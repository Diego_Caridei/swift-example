//
//  ViewController.swift
//  NSBundleExample
//
//  Created by Guybrush_Treepwood on 18/06/15.
//  Copyright (c) 2015 Guybrush_Treepwood. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

      let imageURL =  NSBundle.mainBundle().URLForResource("cat", withExtension: "jpg")
        let data = NSData(contentsOfURL: imageURL!)
        imageView.image = UIImage(data: data!)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

