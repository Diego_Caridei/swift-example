//
//  ViewController.swift
//  ViewCotroller
//
//  Created by Guybrush_Treepwood on 29/05/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        println("View has loaded")
    }

    override func viewWillLayoutSubviews() {
        println("View is about to layout subviews")

    }
    override func viewDidLayoutSubviews() {
        println("View has layed out subwies")

    }
    override func viewWillAppear(animated: Bool) {
        println("View will appear")

    }
    
    override func viewDidAppear(animated: Bool) {
        println("View has appeared")

    }
    
    override func viewWillDisappear(animated: Bool) {
        println("View is about to disapper")

    }
    override func viewDidDisappear(animated: Bool) {
        println("View has disappeared")

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

