//
//  ViewController.swift
//  NSURLSessionMacOSX
//
//  Created by Guybrush_Treepwood on 16/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBOutlet weak var imageView: NSImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        //Set size
        let width = imageView.bounds.width
        let height = imageView.bounds.height
        
        let urlString = "http://placekitten.com/g/\(Int(width))/\(Int(height))"
        
        if let url = NSURL (string: urlString){
            let session = NSURLSession (configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
            let dataTask = session.dataTaskWithURL(url , completionHandler: { (data: NSData? , response: NSURLResponse?, error: NSError?) -> Void in
                if data == nil{
                    self.imageView.image = nil
                }else{
                    if let image = NSImage(data: data!){
                        self.imageView.image = image
                    }
                }
            })
            dataTask.resume()
        }
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

