//
//  ViewController.swift
//  LibraryAccessMusic
//
//  Created by Guybrush_Treepwood on 07/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit
import MediaPlayer
class ViewController: UIViewController {

    var nowPlayingItemChange : AnyObject?
    let player = MPMusicPlayerController.iPodMusicPlayer()
    
    
    @IBOutlet weak var nowPlayingLabel: UILabel!
    
    deinit{
        
     NSNotificationCenter.defaultCenter().removeObserver(self.nowPlayingItemChange!)
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //notifica quando cambia una canzone
        player.beginGeneratingPlaybackNotifications()
        nowPlayingItemChange = NSNotificationCenter.defaultCenter().addObserverForName(MPMusicPlayerControllerNowPlayingItemDidChangeNotification, object: nil, queue: NSOperationQueue.mainQueue(), usingBlock: { (notification) -> Void in
            self.updateNowPlayingItem()
        })
        
        updateNowPlayingItem()
        
    }
    
    func updateNowPlayingItem(){
        if  let nowPlayinItem = player.nowPlayingItem {
        let nowPlayinTitle =  nowPlayinItem.title
        nowPlayingLabel.text = nowPlayinTitle
        
        }else{
            
            nowPlayingLabel.text = "Nothing Play"
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func previous(sender: AnyObject) {
        player.skipToPreviousItem()
    }

    @IBAction func next(sender: AnyObject) {
        player.skipToNextItem()
    }
}

