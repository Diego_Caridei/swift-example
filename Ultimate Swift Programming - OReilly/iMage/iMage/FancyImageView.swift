//
//  FancyImageView.swift
//  iMage
//
//  Created by Guybrush_Treepwood on 02/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

@IBDesignable class FancyImageView: UIView {

    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        let image = UIImage(named: "puppy-uhr1n1.jpg", inBundle: NSBundle(forClass: FancyImageView.self ), compatibleWithTraitCollection: self.traitCollection)
        image?.drawAtPoint(CGPoint(x: 0, y: 0))
        image?.drawInRect(CGRect(x: 10, y: 10, width: 50, height: 24))
        
    }
   

}
