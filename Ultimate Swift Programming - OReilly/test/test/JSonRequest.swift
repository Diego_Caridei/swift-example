//
//  JSonRequest.swift
//  test
//
//  Created by Guybrush_Treepwood on 17/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import Foundation
class JSonRequest: NSObject, NSURLConnectionDelegate {

    var data = NSMutableData()
    var urlPath: String?
    var keyJson = [String] ()
    var dict = NSMutableDictionary()
    func startConnection(){
        urlPath = "http://www.telize.com/geoip"
        var url: NSURL = NSURL(string: urlPath!)!
        var request: NSURLRequest = NSURLRequest(URL: url)
        var connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: false)!
        connection.start()
    }
    
     override init () {
        super.init()
    }
    
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!){
        self.data.appendData(data)
    }
    
   // func print
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        
        if let  dict = NSJSONSerialization.JSONObjectWithData(data, options: nil, error:nil) as! NSMutableDictionary {
            
           println( dict.valueForKey("isp")!)
            
        } else {
            println("Could not read JSON dictionary")
        }
    }
    
}