//
//  FancyBlendingView.swift
//  BlendingView
//
//  Created by Guybrush_Treepwood on 02/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

@IBDesignable class FancyBlendingView: UIView {


    @IBInspectable var color1 : UIColor = UIColor.yellowColor()
    @IBInspectable var  color2 : UIColor = UIColor.blueColor()
    
    override func drawRect(rect: CGRect) {
        let rettangolo = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 50, height: 50))
        let rettangolo2 = UIBezierPath(rect: CGRect(x: 25, y: 25, width: 50, height: 50))

        color1.setFill()
        rettangolo.fill()
        color2.setFill()
        rettangolo2.fillWithBlendMode(kCGBlendModeHardLight, alpha: 0.5)
    }
    

}
