//
//  KeyboardViewController.swift
//  HodorKeyboard
//
//  Created by Guybrush_Treepwood on 01/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

class KeyboardViewController: UIInputViewController {

    @IBOutlet weak var hodorButton: UIButton!
    @IBOutlet var nextKeyboardButton: UIButton!

    var keyboardView : UIView!
    
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
    
        // Add custom view sizing constraints here
    }

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(UINib(nibName: "HodorView", bundle: nil).instantiateWithOwner(self, options: nil)[0] as! UIView)
        nextKeyboardButton.addTarget(self, action: "method", forControlEvents: .TouchUpinside)
        

        
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
    }

    override func textWillChange(textInput: UITextInput) {
        // The app is about to change the document's contents. Perform any preparation here.
    }

    override func textDidChange(textInput: UITextInput) {
        // The app has just changed the document's contents, the document context has been updated.
    
        var textColor: UIColor
        var proxy = self.textDocumentProxy as! UITextDocumentProxy
        if proxy.keyboardAppearance == UIKeyboardAppearance.Dark {
            textColor = UIColor.whiteColor()
        } else {
            textColor = UIColor.blackColor()
        }
        self.nextKeyboardButton.setTitleColor(textColor, forState: .Normal)
    }

}
