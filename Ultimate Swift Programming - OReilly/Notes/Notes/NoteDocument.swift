//
//  NoteDocument.swift
//  Notes
//
//  Created by Guybrush_Treepwood on 24/06/15.
//  Copyright (c) 2015 CarideiSolution. All rights reserved.
//

import UIKit

class NoteDocument: UIDocument {
    var text : String = ""

    override func loadFromContents(contents: AnyObject, ofType typeName: String, error outError: NSErrorPointer) -> Bool {
        
        let dictionary = NSKeyedUnarchiver.unarchiveObjectWithData(contents as! NSData)as! NSDictionary
        
        self.text = dictionary["text"] as? String ?? ""
        
        return true
    }
    
    
    override func contentsForType(typeName: String, error outError: NSErrorPointer) -> AnyObject? {
        
        var dictionary :[String:AnyObject] = ["text":self.text]
        let data = NSKeyedArchiver.archivedDataWithRootObject(dictionary)
        
        return data
    
    }
}
