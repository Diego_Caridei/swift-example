//
//  ViewController.swift
//  UserDefaults
//
//  Created by Guybrush_Treepwood on 16/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var toggleSwitch: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.toggleSwitch.on = NSUserDefaults.standardUserDefaults().boolForKey("toogleSwitch")
        
        
        
    }

    @IBAction func swithTapped(sender: AnyObject) {
        let state = self.toggleSwitch.on
        NSUserDefaults.standardUserDefaults().setBool(state, forKey: "toogleSwitch")
        NSUserDefaults.standardUserDefaults().synchronize()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

