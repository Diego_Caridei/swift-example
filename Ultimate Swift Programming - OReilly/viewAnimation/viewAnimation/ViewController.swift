//
//  ViewController.swift
//  viewAnimation
//
//  Created by Guybrush_Treepwood on 03/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var height : NSLayoutConstraint!
    @IBOutlet weak var widht : NSLayoutConstraint!
    @IBOutlet weak var horizontalCentre : NSLayoutConstraint!
    @IBOutlet weak var top : NSLayoutConstraint!
    
    @IBOutlet weak var animationView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func animate (sender:AnyObject){
        height.constant = 200
        widht.constant = 200
        
        horizontalCentre.constant += 50
        top.constant += 50
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.animationView.backgroundColor=UIColor.orangeColor()
            
            /*
            let centerPoint = CGPoint(x: 150, y: 150)
            let size = CGSize(width: 200, height: 200)
            self.animationView.frame = CGRect(origin: centerPoint, size: size)
            */
            
            self.view.layoutIfNeeded()
            
        },completion:nil)
          
    }
}

