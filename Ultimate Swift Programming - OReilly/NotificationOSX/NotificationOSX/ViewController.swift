//
//  ViewController.swift
//  NotificationOSX
//
//  Created by Guybrush_Treepwood on 15/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    @IBAction func sendNotification(sender: AnyObject) {
        let notificationCentre  = NSUserNotificationCenter.defaultUserNotificationCenter()
        
        let notification = NSUserNotification()
        notification.title = "Title"
        notification.informativeText = "Wake up"
        notification.hasActionButton = true
        notification.actionButtonTitle = "Go Away"
        
        notification.deliveryDate = NSDate(timeIntervalSinceNow: 2)
        notificationCentre.scheduleNotification(notification)
        
    }


}

