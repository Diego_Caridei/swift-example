//
//  AppDelegate.swift
//  CostraintsOSX
//
//  Created by Guybrush_Treepwood on 31/05/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!


    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

