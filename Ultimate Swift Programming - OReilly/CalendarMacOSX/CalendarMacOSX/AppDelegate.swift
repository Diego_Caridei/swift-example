//
//  AppDelegate.swift
//  CalendarMacOSX
//
//  Created by Guybrush_Treepwood on 18/06/15.
//  Copyright (c) 2015 Guybrush_Treepwood. All rights reserved.
//

import Cocoa
import EventKit

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!

    var eventStore : EKEventStore?
    
    
    
    
    func applicationDidFinishLaunching(aNotification: NSNotification) {
        eventStore = EKEventStore()
        eventStore?.requestAccessToEntityType(EKEntityTypeEvent, completion: { (success, error) -> Void in
            
           print("Access granted?\(success)")
           var calendars = self.eventStore?.calendarsForEntityType(EKEntityTypeEvent)
           let startDate = NSDate()
           let dateCalculation = NSCalendar.currentCalendar()
           let fiveHours = NSDateComponents()
            fiveHours.hour = 5
           let endDate = dateCalculation.dateByAddingComponents(fiveHours, toDate: startDate, options: NSCalendarOptions())
            // let endDate = startDate.dateByAddingTimeInterval(5*60*60)
            for calendar in (calendars as! [EKCalendar]){
                println("Calendar: \(calendar)")
            }
            
            let searchPredicate = self.eventStore?.predicateForEventsWithStartDate(startDate, endDate: endDate, calendars: calendars)
            let events = self.eventStore?.eventsMatchingPredicate(searchPredicate)
            
            for i in (events as! [EKEvent]){
                println("Event that starts at:\(i.startDate)")
            }
            
            
            
        })
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

