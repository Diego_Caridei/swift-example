//
//  ViewController.swift
//  BezierPath
//
//  Created by Guybrush_Treepwood on 02/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var francyView: FancyBezyerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.addSubview(francyView)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

