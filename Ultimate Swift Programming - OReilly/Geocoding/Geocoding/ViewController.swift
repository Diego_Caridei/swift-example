//
//  ViewController.swift
//  Geocoding
//
//  Created by Guybrush_Treepwood on 15/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit
import CoreLocation


class ViewController: UIViewController {

    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    var geocoder : CLGeocoder?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        geocoder = CLGeocoder()
        
    
    }

    @IBAction func geocode(sender: AnyObject) {
        /*
        if let text = textField.text{
            
            geocoder?.geocodeAddressString(text, completionHandler: { (placeMakers : [AnyObject]!, error: NSError!) -> Void in
                if let thePlacemark = placeMakers.last as? CLPlacemark
                {
                    self.locationLabel.text = "lat: \(thePlacemark.location.coordinate.latitude) long:\(thePlacemark.location.coordinate.longitude)"
                    
                    
                }
                
            })
        }
*/
            
            let theLocation = CLLocation(latitude: 38.4111771, longitude: -122.84110635)
            self.geocoder?.reverseGeocodeLocation(theLocation, completionHandler:  { (placeMakers : [AnyObject]!, error: NSError!) -> Void in
                if let thePlacemark = placeMakers.last as? CLPlacemark
                {
                   self.textField.text = thePlacemark.thoroughfare
                }
                
            })

        }
        
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
        
    }


}

