//
//  FancyBezyerView.swift
//  BezierPath
//
//  Created by Guybrush_Treepwood on 02/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

@IBDesignable class FancyBezyerView: UIView {

    
    
    @IBInspectable var fillColor = UIColor.greenColor()
    @IBInspectable var strokeColor = UIColor.blackColor()
    @IBInspectable var lineWidth : CGFloat = 3.0
    
    @IBInspectable var curveControlPoint1 : CGPoint = CGPoint(x: 150, y: 25)
    @IBInspectable var curveControlPoint2 :CGPoint = CGPoint(x: 50, y: 75)
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        fillColor.setFill()
        strokeColor.setStroke()
        var line = UIBezierPath()
        line.moveToPoint(CGPoint(x: 0, y: 0))
        line.addLineToPoint(CGPoint(x: 50, y: 50))
        line.lineWidth = self.lineWidth
        line.stroke()
        let quadCurve = UIBezierPath()
        quadCurve.lineWidth = self.lineWidth
        quadCurve.moveToPoint(CGPoint(x: 50, y: 0))
        quadCurve.addQuadCurveToPoint(CGPoint(x: 50, y: 100), controlPoint: CGPoint(x: 150, y: 150))
        quadCurve.stroke()
        let bezierPath = UIBezierPath()
        bezierPath.lineWidth = self.lineWidth
        bezierPath.moveToPoint(CGPoint(x: 100, y: 0))
        bezierPath.addCurveToPoint(CGPoint(x: 100, y: 100), controlPoint1: curveControlPoint1, controlPoint2: curveControlPoint2)
        bezierPath.stroke()
        
    }


}
