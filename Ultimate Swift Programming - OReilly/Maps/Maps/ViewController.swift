//
//  ViewController.swift
//  Maps
//
//  Created by Guybrush_Treepwood on 14/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController,MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Tipo di mappa
        mapView.mapType = MKMapType.Hybrid
        let centre = CLLocationCoordinate2D(latitude: 40.85685, longitude: -14.28446)
        mapView.centerCoordinate = centre
        
        //Point annotation
        let annotation = MKPointAnnotation ()
        annotation.title = "Parthenope University"
        annotation.subtitle = "My university"
        annotation.coordinate = centre
        mapView.addAnnotation(annotation)
        
        
        let overlay =  MKCircle(centerCoordinate: centre, radius: 1000)
        mapView.addOverlay(overlay)
        mapView.delegate = self
    }
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        let render = MKCircleRenderer(circle: overlay as! MKCircle)
        render.lineWidth = 20
        render.fillColor = UIColor.greenColor()
        return render
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

