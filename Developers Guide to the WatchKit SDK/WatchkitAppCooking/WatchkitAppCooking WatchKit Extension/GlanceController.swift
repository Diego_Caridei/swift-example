//
//  GlanceController.swift
//  WatchkitAppCooking WatchKit Extension
//
//  Created by Guybrush_Treepwood on 24/06/15.
//  Copyright (c) 2015 CarideiSolution. All rights reserved.
//

import WatchKit
import Foundation


class GlanceController: WKInterfaceController {

    let groupName = "group.com.caridei.watchkitCooking"
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        let shareDefaults = NSUserDefaults(suiteName: groupName)
        let weatherData = shareDefaults?.valueForKey("weather") as? String
        if let receievedWeatherData = weatherData {
            println(receievedWeatherData)
        }

    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
