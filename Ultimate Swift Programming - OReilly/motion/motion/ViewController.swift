//
//  ViewController.swift
//  motion
//
//  Created by Guybrush_Treepwood on 15/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit
import CoreMotion

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    let motionManager =  CMMotionManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        motionManager.startDeviceMotionUpdatesToQueue(NSOperationQueue.mainQueue(), withHandler: { (motion , error) -> Void in
            /*
            let userAccelaration = motion.userAcceleration
            let xMovement = userAccelaration.x

            */
            let attitude = motion.attitude
            let yaw = attitude.yaw
            
            let yawString = NSString(format: "%.2f", yaw)
            self.label.text = yawString as String
            
        })
        self.motionManager.stopDeviceMotionUpdates()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }


}

