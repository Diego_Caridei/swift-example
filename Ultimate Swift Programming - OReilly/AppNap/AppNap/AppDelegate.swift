//
//  AppDelegate.swift
//  AppNap
//
//  Created by Guybrush_Treepwood on 29/05/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!


    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
        let coda = NSOperationQueue.mainQueue()
        var token = NSProcessInfo.processInfo()
        token.beginActivityWithOptions(NSActivityOptions.UserInitiated, reason: "Very important stuff")
        
        coda.addOperationWithBlock { () -> Void in
            println("Doing some very important stuff")
            NSProcessInfo.processInfo().endActivity(token)
        }
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }
    func applicationDidChangeOcclusionState(notification: NSNotification) {
        if NSApp.occlusionState & NSApplicationOcclusionState.Visible != nil{
            NSLog("We are application in the foreground")
         
        }else{
            NSLog("We are application in the background")
        }
    }


}

