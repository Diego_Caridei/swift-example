//
//  ViewController.swift
//  SharingApp
//
//  Created by Guybrush_Treepwood on 07/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func share(sender: AnyObject) {
        let ourActivity = GreatActivity()
        let activity = UIActivityViewController(activityItems: [self.textField.text], applicationActivities: [ourActivity])
        self.presentViewController(activity, animated: true, completion: nil)
        
    }


}

