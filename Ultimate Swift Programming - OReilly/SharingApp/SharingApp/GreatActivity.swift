//
//  GreatActivity.swift
//  SharingApp
//
//  Created by Guybrush_Treepwood on 07/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

class GreatActivity: UIActivity {
    var avtivityItem : [String] = []
    
    override func activityType() -> String? {
        return "UIActivityTypeGreatActivity"
    }
    
    override func activityImage() -> UIImage? {
        return nil //oppure return un image
    }
    
    override func activityTitle() -> String? {
        return "Great Activity"
    }
    
    override class func activityCategory() ->UIActivityCategory{
        return UIActivityCategory.Share
    }
   
    override func canPerformWithActivityItems(activityItems: [AnyObject]) -> Bool {
        for item in activityItems{
            if !( item is String){
                return false
            }
            
        }
        return true
    }
    
    override func prepareWithActivityItems(activityItems: [AnyObject]) {
        self.avtivityItem = activityItems as! [String]
    }
    
    override func performActivity() {
        for item in self.avtivityItem{
            println(item)
        }
        activityDidFinish(true)
    }
}
