//
//  FancyView.swift
//  OSX_Animation
//
//  Created by Guybrush_Treepwood on 03/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import Cocoa

@IBDesignable class FancyView: NSView {

    @IBInspectable var color : NSColor = NSColor.greenColor()
    
    override func drawRect(dirtyRect: NSRect) {
        super.drawRect(dirtyRect)
        
        let rect = NSInsetRect(self.bounds, 20, 20)
        let path = NSBezierPath(roundedRect: rect, xRadius: 10.0, yRadius: 10.0)
        color.setFill()
        path.fill()
        
    }
    
}
