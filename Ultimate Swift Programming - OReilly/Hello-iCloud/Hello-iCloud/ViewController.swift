//
//  ViewController.swift
//  Hello-iCloud
//
//  Created by Guybrush_Treepwood on 23/06/15.
//  Copyright (c) 2015 Caridei Solution. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    var  keyValueStoreWasChangedObserver : AnyObject?
    @IBOutlet weak var segment: UISegmentedControl!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //verifico se ho accesso ad iCloud
        if NSFileManager.defaultManager().ubiquityIdentityToken == nil{
            self.segment.enabled = false
        } else{
             self.segment.enabled = true
        }
        self.keyValueStoreWasChangedObserver = NSNotificationCenter.defaultCenter().addObserverForName(NSUbiquitousKeyValueStoreDidChangeExternallyNotification, object: nil, queue: NSOperationQueue.mainQueue(), usingBlock: { (notification) -> Void in
            
            self.updateSelectedSegment()
        })
        
        self.updateSelectedSegment()

    }

    func updateSelectedSegment (){
        if let selectIndex = NSUbiquitousKeyValueStore.defaultStore().objectForKey("selectedIndex") as? Int {
            
            self.segment.selectedSegmentIndex = selectIndex
        } else {
            
            self.segment.selectedSegmentIndex = -1
        }
        
    }
    
    @IBAction func valueChanged(sender: AnyObject) {
        
        NSUbiquitousKeyValueStore.defaultStore().setObject(segment.selectedSegmentIndex, forKey: "selectedIndex")
        
        NSUbiquitousKeyValueStore.defaultStore().synchronize()
         
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

