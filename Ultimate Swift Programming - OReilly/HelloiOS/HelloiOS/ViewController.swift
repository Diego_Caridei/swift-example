//
//  ViewController.swift
//  HelloiOS
//
//  Created by Guybrush_Treepwood on 29/05/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    @IBAction func tapButton(sender: AnyObject) {
        label.text = "Ciao mondo"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

