//
//  ViewController.swift
//  Speaking
//
//  Created by Guybrush_Treepwood on 12/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit
import AVFoundation


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func speak(sender: AnyObject) {
        var sythesizer = AVSpeechSynthesizer()
        let string = "Hello World"
        var utterance = AVSpeechUtterance(string: string)
        utterance.rate = 0.18
        sythesizer.speakUtterance(utterance)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

