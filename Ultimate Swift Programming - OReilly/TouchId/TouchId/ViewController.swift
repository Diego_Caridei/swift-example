//
//  ViewController.swift
//  TouchId
//
//  Created by Guybrush_Treepwood on 15/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit
import LocalAuthentication

class ViewController: UIViewController {

    let autationticationContext = LAContext()
    
    @IBAction func authenticate(sender: AnyObject) {
        let policy  = LAPolicy.DeviceOwnerAuthenticationWithBiometrics
        var error : NSError? = nil
        let canAuthenticate = autationticationContext.canEvaluatePolicy(policy, error: &error)
        if canAuthenticate == false{
            println("Can't use authentication system \(error)")
            return
        }
        let authenticationReason = "Give me your fingernails!"
        autationticationContext.evaluatePolicy(policy, localizedReason: authenticationReason) { (succeded, error) -> Void in
            if succeded{
                println("Authenticated")
            }
            else{
                println("Not authenticated \(error)")

            }
            if error.code == LAError.UserFallback.rawValue{
                //deal with error
            }
            if error.code == LAError.UserCancel.rawValue{
                //user cancelled
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

