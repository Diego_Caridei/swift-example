//
//  ViewController.swift
//  DynamicsAnimation
//
//  Created by Guybrush_Treepwood on 03/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    var snap:UISnapBehavior?
    @IBOutlet weak var physicsView: UIView!
    var dynamicAnimator = UIDynamicAnimator()

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        dynamicAnimator = UIDynamicAnimator (referenceView: self.view)
        
        let gravityBehavior = UIGravityBehavior(items: [physicsView])
        dynamicAnimator.addBehavior(gravityBehavior)
        
        let collisionBehavior = UICollisionBehavior(items: [physicsView])
        collisionBehavior.translatesReferenceBoundsIntoBoundary = true
        dynamicAnimator.addBehavior(collisionBehavior)
        
        
    }
    
    @IBAction func tapped(sender: AnyObject) {
        let tap = sender as! UITapGestureRecognizer
        if tap.state == UIGestureRecognizerState.Ended{
            let point = tap.locationInView(self.view)
            self.dynamicAnimator.removeBehavior(self.snap)
            self.snap = UISnapBehavior(item: physicsView, snapToPoint: point)
            self.dynamicAnimator.addBehavior(self.snap)
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

