//
//  ViewController.swift
//  Camera
//
//  Created by Guybrush_Treepwood on 07/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func openPhotoLibrary(sender: AnyObject) {
        
        let picker = UIImagePickerController()
        let sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        if UIImagePickerController.isSourceTypeAvailable(sourceType){
            picker.sourceType = sourceType
            picker.delegate = self
            self.presentViewController(picker, animated: true, completion: nil)
        }
    }
    
    
    
    @IBAction func useCamera(sender: AnyObject) {
        
        let picker = UIImagePickerController()
        let sourceType = UIImagePickerControllerSourceType.Camera
        
        if UIImagePickerController.isSourceTypeAvailable(sourceType){
            
            picker.sourceType = sourceType
            
            let frontCamera = UIImagePickerControllerCameraDevice.Front
            if UIImagePickerController.isCameraDeviceAvailable(frontCamera){
                picker.cameraDevice = frontCamera
                picker.delegate = self
                self.presentViewController(picker, animated: true, completion: nil)
                
            }
        }
    }
    
    //Delegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageView.image = image
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
}

