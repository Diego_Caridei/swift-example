//
//  main.c
//  SommaDiagonale
//
//  Created by Guybrush_Treepwood on 02/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

void stampa(int **matrice,int riga ,int colonna){
    for (int i=0; i<riga; i++) {
        for (int j=0; j< colonna; j++) {
            printf("%d \t",matrice[i][j]);
        }
        printf("\n");
    }
}


int **matrice=NULL;
int somma=0;
int main(int argc, const char * argv[]) {
    int riga = 3;
    int colonna=3;
    matrice = (int **)malloc(sizeof(int*)*riga);
    for (int i=0; i<riga; i++) {
        matrice[i]=(int*)malloc(sizeof(int)*colonna);
        for (int  j=0; j<colonna; j++) {
            matrice[i][j]= 2+rand()%10;
        }
    }
    printf("\n");
    
    //Somma diagonale
    stampa(matrice, riga, colonna);
    for (int i=0; i<riga; i++) {
        somma=somma+matrice[i][i];
    }
    
    int somma2=0;
    
//    int d,s;
//    for( int i = 0; i < riga; i++){
//        d += matrice[i][i];   // Diagonale Principale
//        s += matrice[i][riga-i-1]; //Diagonale Secondaria
//    }
//
    int sommaR=0;
    int sommaM=0;
    
    for (int i=0; i< riga; i++) {
        for (int j=0; j<colonna; j++) {
            sommaM=sommaM+matrice[i][j];
        }
    }
    printf("%d",sommaM);
    
    printf("\n");
//   printf("\n%d %d",d,s);
    return 0;
}
