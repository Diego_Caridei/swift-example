//
//  main.c
//  File
//
//  Created by Guybrush_Treepwood on 15/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <sys/stat.h>
pthread_mutex_t mutext = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condizione = PTHREAD_COND_INITIALIZER;
int numThread;


void *funzione1 (void *param){
    
    char *buff = malloc(sizeof(int));
    int fd = open("file.txt", O_WRONLY|O_APPEND);
    pthread_mutex_lock(&mutext);
    int indice = *(int*)param;
    sprintf(buff, "%d",indice);
    write(fd, buff, sizeof(int));
    numThread--;
    pthread_mutex_unlock(&mutext);
    if (numThread<=0) {
        close(fd);
        pthread_mutex_lock(&mutext);
        pthread_cond_signal(&condizione);
        pthread_mutex_unlock(&mutext);

    }
    
    pthread_exit(0);
}

void *funzione2 (void *param){
    pthread_mutex_lock(&mutext);
    while (numThread>0) {
        pthread_cond_wait(&condizione, &mutext);
    }
    pthread_mutex_unlock(&mutext);
    char *buff = malloc(sizeof(int));
    int fd = open("file.txt",O_RDONLY);
    ssize_t    byteRead=0;
    do{
        byteRead=read(fd, buff, sizeof(int));
        printf("%s\n",buff);
        
    }while (byteRead>0);
    
    pthread_exit(0);
}



int main(int argc, const char * argv[]) {

    int n = 10;
    numThread = n;
    pthread_t threads[n];
    unlink("file.txt");
    int fd = open("file.txt", O_CREAT,S_IRWXU|S_IRWXG|S_IRWXO);
    close(fd);
    for (int i=0; i<n; i++) {
        int *indice = malloc(sizeof(int));
        *indice=i;
        pthread_create(&threads[i], NULL, funzione1, indice);
    }
    pthread_create(&threads[n], NULL, funzione2, NULL);

    for (int i=0; i<n; i++) {
        pthread_join(threads[i], NULL);
    }
    return 0;
}
