//
//  ViewController.swift
//  AVFoundation_iOS
//
//  Created by Guybrush_Treepwood on 04/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
class ViewController: AVPlayerViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let contentURL = NSBundle.mainBundle().URLForResource("video2", withExtension: "m4v")
        let player = AVPlayer(URL: contentURL)
        self.player = player
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

