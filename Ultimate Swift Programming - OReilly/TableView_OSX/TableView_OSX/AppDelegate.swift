//
//  AppDelegate.swift
//  TableView_OSX
//
//  Created by Guybrush_Treepwood on 31/05/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate,NSTableViewDataSource,NSTableViewDelegate {

    @IBOutlet weak var window: NSWindow!
    var tableData = [("Greta's Lament",289),("The Signal",309),("Resurection",221)]

    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }
    
    
    func tableView(tableView: NSTableView, viewForTableColumn tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        var cell = tableView.makeViewWithIdentifier(tableColumn!.identifier, owner: self) as! NSTableCellView
        
        let song = tableData[row]
        
        switch tableColumn!.identifier{
            case "Title":
            cell.textField?.stringValue = song.0
            
            case "Duration":
            cell.textField?.stringValue = NSString(format: "%i:%02i", song.1 / 60,song.1%60) as String
        default:
         cell.textField?.stringValue = ""
        }
        
        
        return cell
    }
    
    func numberOfRowsInTableView(tableView: NSTableView) -> Int {
        return tableData.count
    }

}

