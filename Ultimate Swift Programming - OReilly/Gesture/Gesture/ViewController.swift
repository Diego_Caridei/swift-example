//
//  ViewController.swift
//  Gesture
//
//  Created by Guybrush_Treepwood on 31/05/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBAction func swippedDoqn(sender: AnyObject) {
        println("Swipped down")

    }
    @IBAction func swipped(sender: AnyObject) {
        println("Swipped up")
    }
    
    
    @IBAction func pinch(sender: AnyObject) {
        println("Pinch detected")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        //Personalizzare gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: "tapped:")
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func tapped(recognizer:UIGestureRecognizer){
        if recognizer.state == UIGestureRecognizerState.Ended{
            println("Tapped")
        }
        
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
       // println("Touch  began \(touches)")
        println("Touch  began \(touches)")
       
    }

    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        println("Touch moved")
    }
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        println("Touch ended")

    }
    override func touchesCancelled(touches: Set<NSObject>!, withEvent event: UIEvent!) {
        println("Touch cancelled")

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

