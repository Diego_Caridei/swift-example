//: Playground - noun: a place where people can play

import UIKit


protocol Drivable{
    func drive()
}


//Visibile solo in questo modulo
internal class Veicolo :Drivable {
    func drive(){
        println("Sto guidando sono, sono un veicolo generico")
    }
    
    var numRuote :Int?
    
}



internal class Car :Veicolo{
    
    
    var colol : UIColor? {
        willSet(newValue){
         println("Sto per cambiare colore con \(newValue)")
        }
        didSet(oldValue){
            
        }
    }
    
    var speed :Int{
        get{
            return self.speed
        }
        set{
            println("")
        }
    }
    
    override init(){
        
    }
    
    deinit{
        
    }
    
    internal override func drive(){
        println("sto guidando \(self.speed) KM")
    }
}

//Estensione
extension Int : Drivable {
    func drive() {
        println("I'm the number \(self)")
    }
}

struct Vettore {
    var x : Int
    var y : Int
    func magnitute () -> Float {
        return sqrt (Float (x) + Float (y))
    }
}

var newCar =  Car
var vet = Vettore (x: 3, y: 2)
println(vet.magnitute())

var auto  = Car()
auto.speed = 20
auto.drive()
var pippo : Drivable?
pippo  = 42
42.drive()

