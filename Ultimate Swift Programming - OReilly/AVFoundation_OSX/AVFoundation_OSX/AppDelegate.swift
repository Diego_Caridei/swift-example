//
//  AppDelegate.swift
//  AVFoundation_OSX
//
//  Created by Guybrush_Treepwood on 03/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import Cocoa
import AVFoundation
import AVKit

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!

    @IBOutlet weak var playerView: AVPlayerView!
    @IBOutlet weak var customView: NSView!
    
    
    var player : AVPlayer?

    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
        
        //Tramite NSView
        let contentURL = NSBundle.mainBundle().URLForResource("video2", withExtension: "m4v")
        player = AVPlayer(URL: contentURL)
        
        var playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = customView.bounds
        playerLayer.autoresizingMask = CAAutoresizingMask.LayerHeightSizable | CAAutoresizingMask.LayerWidthSizable
        
        customView.layer?.addSublayer(playerLayer)
        
        
        //Tramite AVPlayerView
        playerView.player = player
        
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }

    @IBAction func playVideo(sender: AnyObject) {
        player?.play()
    }

    @IBAction func rewindVideo(sender: AnyObject) {
        player?.seekToTime(kCMTimeZero)
    }
    @IBAction func slowMotion(sender: AnyObject) {
        player?.rate = 0.25
    }
    
  
    
}

