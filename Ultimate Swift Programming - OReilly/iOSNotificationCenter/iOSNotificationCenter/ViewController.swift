//
//  ViewController.swift
//  iOSNotificationCenter
//
//  Created by Guybrush_Treepwood on 29/05/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var backgroundObserver:AnyObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notificationCenter = NSNotificationCenter.defaultCenter()
        let operationQueue = NSOperationQueue.mainQueue()
        self.backgroundObserver = notificationCenter.addObserverForName(UIApplicationDidEnterBackgroundNotification, object: nil, queue: operationQueue, usingBlock: { (notification:NSNotification!) -> Void in
            println("Siamo entrati in background")
        })
        
    }
    //libero dopo il notification
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self.backgroundObserver!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

