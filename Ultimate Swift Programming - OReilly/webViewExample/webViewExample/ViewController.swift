//
//  ViewController.swift
//  webViewExample
//
//  Created by Guybrush_Treepwood on 01/06/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIWebViewDelegate{

    @IBOutlet weak var buttonRefresh: UIButton!
    @IBOutlet weak var buttoForward: UIButton!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var webView: UIWebView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        buttonBack.enabled = false
        buttoForward.enabled = false
        
        if let url = NSURL(string: "http://www.iprog.it/blog"){
            let request = NSURLRequest(URL: url)
            self.webView.loadRequest(request)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func refresh(sender: AnyObject) {
        webView.reload()
        
    }
    @IBAction func forward(sender: AnyObject) {
        webView.goForward()
        
    }
    @IBAction func back(sender: AnyObject) {
        webView.goBack()
    }
    
    
    func webViewDidFinishLoad(webView: UIWebView) {
        buttonBack.enabled = webView.canGoBack
        buttoForward.enabled = webView.canGoForward
        
    }
    
    @IBAction func runJS(sender: AnyObject) {
        webView.stringByEvaluatingJavaScriptFromString("alert('Hello World')")
    }
    
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.LinkClicked {
            UIApplication.sharedApplication().openURL(request.URL!)
            return false
        }
        
        return true
    }
}

