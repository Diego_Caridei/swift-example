//
//  AppDelegate.swift
//  OperationQueue
//
//  Created by Guybrush_Treepwood on 29/05/15.
//  Copyright (c) 2015 DiegoCaridei. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!


    func applicationDidFinishLaunching(aNotification: NSNotification) {

        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
            println("Hello from the main queue!")
        }
        
        NSOperationQueue().addOperationWithBlock { () -> Void in
            println("Hello from background queue")
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                println("Hello againg from the main queue")
            })
        }
        
        let operation = NSBlockOperation { () -> Void in
            println("Hello from the block operation")
        }
        
        let myNewImportantOperation = NSBlockOperation { () -> Void in
            println("This is operation runs first")
        }
        
    
        
        operation.qualityOfService = NSQualityOfService.Background
        
        NSOperationQueue.mainQueue().addOperation(operation)
    
    
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

