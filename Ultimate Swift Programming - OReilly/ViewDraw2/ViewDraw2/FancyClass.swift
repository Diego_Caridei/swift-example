//
//  FancyClass.swift
//  
//
//  Created by Guybrush_Treepwood on 01/06/15.
//
//

import UIKit


@IBDesignable
class FancyClass: UIView {

    var cornerRadius : CGFloat = 20.0
    var color = UIColor.greenColor()

    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    
        var rettangolo = UIBezierPath(roundedRect: self.bounds, cornerRadius: cornerRadius)
        color.setFill()
        rettangolo.fill()
        
    }


}
