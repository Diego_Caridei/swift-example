//
//  ViewController.swift
//  AddressBook
//
//  Created by Guybrush_Treepwood on 18/06/15.
//  Copyright (c) 2015 Guybrush_Treepwood. All rights reserved.
//

import UIKit
import AddressBookUI
import AddressBook

class ViewController: UIViewController, ABPeoplePickerNavigationControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func findPeoble(sender: AnyObject) {
        var peoplePicker = ABPeoplePickerNavigationController()
        peoplePicker.peoplePickerDelegate = self
        self.presentViewController(peoplePicker, animated: true, completion: nil)
    }
    
    //Delegate
    func peoplePickerNavigationControllerDidCancel(peoplePicker: ABPeoplePickerNavigationController!) {
        peoplePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func peoplePickerNavigationController(peoplePicker: ABPeoplePickerNavigationController!, didSelectPerson person: ABRecord!) {
        firstNameLabel.text = ABRecordCopyValue(person, kABPersonFirstNameProperty).takeRetainedValue() as? String
        lastNameLabel.text = ABRecordCopyValue(person, kABPersonLastNameProperty).takeRetainedValue() as? String
        peoplePicker.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    


}

